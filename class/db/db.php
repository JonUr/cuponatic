<?php
    class Db {
        public $Server;
        public $User;
        public $Pass;
        public $Database;

        public $Link;

        public $CrearLog;
        // The database connection
        protected static $connection;
        protected static $connectionDiscador;
        protected static $connectionDiscadorAsterisk;

        /**
         * Connect to the database
         *
         * @return bool false on failure / mysqli MySQLi object instance on success
         */
        public function __construct(){
            $Conf = parse_ini_file("conf.ini");
            
            $this->Server = $Conf["serverDB"];
            $this->Pass = $Conf["passDB"];
            $this->User = $Conf["userDB"];
            $this->Database = $Conf["DB"];

            if (!isset($_SESSION)){
                session_start();
            }
        }

        public function connect() {
            $ToReturn =  "";
            if(!isset(self::$connection)) {
                //self::$connection = mysql_connect($this->Server,$this->User,$this->Pass);
                self::$connection = mysqli_connect($this->Server,$this->User,$this->Pass);
                mysqli_select_db(self::$connection, $this->Database);
                $this->query("SET NAMES 'utf8'");
            }

            // If connection was not successful, handle the error
            if(self::$connection === false) {
                // Handle error - notify administrator, log to a file, show an error screen, etc.
                $ToReturn = false;
            }
            $ToReturn = self::$connection;
            return $ToReturn;
        }

        /**
         * Query the database
         *
         * @param $query The query string
         * @return mixed The result of the mysqli::query() function
         */
        public function query($query) {
            // Connect to the database
            $connection = $this -> connect();
            // Query the database
            $result = mysqli_query($connection, $query);
            return $result;
        }

        /**
         * Fetch rows from the database (SELECT query)
         *
         * @param $query The query string
         * @return bool False on failure / array Database rows on success
         */
        public function select($query) {
            $rows = array();
            $result = $this -> query($query);
            if($result === false) {
                return false;
            }
            while ($row = mysqli_fetch_assoc($result)) {
                $rows[] = $row;
            }
            
            return $rows;
        }
        public function getLastID(){
            $connection = $this -> connect();
            return mysqli_insert_id($connection);
        }
    }
?>
