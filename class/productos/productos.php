<?php
    class Productos{
        
        function __construct(){
        
        }
        function getProductos($InputText){
            $db = new DB();
            $SqlProductos = "select * from productos where titulo like '%".$InputText."%'";
            $Productos = $db->select($SqlProductos);
            return $Productos;
        }
        function saveSearch($searchText){
            $db = new DB();
            $Productos = $this->getProductos($searchText);
            foreach($Productos as $Producto){
                $idProducto = $Producto["id"];
                $SqlInsert = "insert into productosbuscados (palabra,idProducto,fecha) values ('".$searchText."','".$idProducto."',NOW())";
                $Insert = $db->query($SqlInsert);
            }
        }
        function getMostSearchedProducts($CantidadProductos){
            $db = new DB();
            $SqlProductos = "
                                SELECT
                                    productos.*
                                FROM
                                    (
                                        SELECT
                                            idProducto,
                                            count(*) as Cantidad
                                        FROM
                                            productosbuscados
                                        GROUP BY
                                            idProducto
                                        LIMIT ".$CantidadProductos."
                                    ) ProductosCantidades
                                        INNER JOIN productos on productos.id = ProductosCantidades.idProducto
                                ORDER BY
                                    Cantidad DESC";
            $Productos = $db->select($SqlProductos);
            return $Productos;
        }
        function getMostSearchedWordsByProduct($idProducto, $CantidadPalabras){
            $db = new DB();
            $SqlPalabras = "
                                SELECT
                                    GROUP_CONCAT(palabra) as Palabras
                                FROM
                                    (
                                        SELECT
                                            palabra,
                                            count(*) as Cantidad
                                        FROM
                                            productosbuscados
                                        WHERE
                                            idProducto='".$idProducto."'
                                        GROUP BY
                                            palabra
                                        ORDER BY
                                            COUNT(*) DESC
                                        LIMIT ".$CantidadPalabras."
                                    ) PalabrasCantidades";
            $Palabras = $db->select($SqlPalabras);
            return $Palabras;
        }
        function getMostSearchedProductsAndWords($CantidadProductos, $CantidadPalabras){
            $ToReturn = array();
            $Productos = $this->getMostSearchedProducts($CantidadProductos);
            foreach($Productos as $Producto){
                $ArrayTmp = array();
                $idProducto = $Producto["id"];
                $nombreProducto = $Producto["titulo"];
                $Palabras = $this->getMostSearchedWordsByProduct($idProducto,$CantidadPalabras);
                $Palabras = $Palabras[0]["Palabras"];
                $ArrayTmp["Producto"] = $nombreProducto;
                $ArrayTmp["Palabras"] = $Palabras;
                array_push($ToReturn,$ArrayTmp);
            }
            return $ToReturn;
        }
    }
?>