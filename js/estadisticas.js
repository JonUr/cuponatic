$(document).ready(function(){
    getMostSearchedProducts();
    function getMostSearchedProducts(){
        $.ajax({
			type: "POST",
			url: "includes/productos/mostSearchedProducts.php",
			data: {
                cantProductos: 20,
                cantPalabras: 5
			},
			async: false,
			success: function(data){
                if(isJson(data)){
                    var Productos = JSON.parse(data);
                    $.each(Productos, function( index, Producto ) {
                        var Titulo = Producto.Producto;
                        var Palabras = Producto.Palabras;
                        var HTML = "<tr><td>"+Titulo+"</td><td>"+Palabras+"</td></tr>";
                        $("#MostSearchedProducts").append(HTML);
                    });
                }
			},
			error: function(){
			}
		});
    }
});