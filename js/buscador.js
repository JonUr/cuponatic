$(document).ready(function(){
    
    $("button[name='searchButton']").click(function(){
        findProduct();
    });

    function findProduct(){
        var InputText = $("input[name='inputSearch']").val();
        $.ajax({
			type: "POST",
			url: "includes/productos/search.php",
			data: {
                keyword: InputText
			},
			async: false,
			success: function(data){
				if(isJson(data)){
					var Productos = JSON.parse(data);
					if(Productos.length > 0){
						saveSearch(InputText);
						$(".ListaProductos").html("");
						$.each(Productos, function( index, Producto ) {
							var Html = $("#ProductoTemplate").html();
							Html = Html.replace("{TITULO}",Producto.titulo);
							Html = Html.replace("{PRECIO}",Producto.precio);
							Html = Html.replace("{TAGS}",Producto.tags);
							Html = Html.replace("{IMAGEN}",Producto.imagen);
							$(".ListaProductos").append(Html);
							
						});
					}
				}
			},
			error: function(){
			}
		});
    }
    function saveSearch(searchText){
        $.ajax({
			type: "POST",
			url: "includes/productos/saveSearch.php",
			data: {
                searchText: searchText
			},
			async: false,
			success: function(data){
			},
			error: function(){
			}
		});
    }
});