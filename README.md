**Manual de uso de Sistema de Prueba Cuponatic**

Una vez clonado el proyecto debe entrar en su Localhost para poder acceder a las vistas del proyecto

---

## Index

Pantalla principal para navegar por la aplicacion web.

---

## buscador.html

Vista para buscar productos en la base de datos y mostrarlos en pantalla.

1. Ingresar palabra a buscar en el campo de texto
2. Hacer click en el boton Buscar
3. Vizualizar los productos encontrados en la base de datos.

Adicional a Esto, tambien se guarda un historial de las palabras buscadas y los productos asociados a esas palabras.

---

## estadisticas.html

Vista para visualizar los 20 productos mas buscados con las 5 palabras mas buscadas de mencionados productos.

---

## Configuracion de Base de datos.

Buscar archivo ubicado en la ruta "class/db/" y editar el archivo **conf.ini** con los datos correspondietes a la configuracion de su motor de base de datos.

Ejecutar **.sql** ubicado en la carpeta "Database Dump" 